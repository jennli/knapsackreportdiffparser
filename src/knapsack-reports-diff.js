import Chart from 'chart.js/auto'
import * as source from '../chart_data/report-diff.json';

(async function () {
  let rawData = source.data;
  const data = rawData.slice(0, 20) // ensure readability of chart output
  // console.log(data)
  const red = 'rgb(255, 99, 132)';
  const blue = 'rgb(54, 162, 235)';

  new Chart(
    document.getElementById('knapsack-reports-diff'),
    {
      type: 'bar',
      data: {
        labels: data.map(row => row.test_file),
        datasets: [
          {
            label: 'job report',
            data: data.map(row => row.actual),
            backgroundCorlor: red,
          },
          {
            label: 'master report',
            data: data.map(row => row.expected),
            backgroundCorlor: blue,
          }
        ]
      },
      options: {
        plugins: {
          title: {
            display: true,
            text: 'Test Duration Regression Top 20 Slowest Spec'
          },
          subtitle: {
            display: true,
            text: ' master report vs job report'
          }
        },
        interaction: {
          mode: 'index'
        },
        scales: {
          y: {
            ticks: {
              // Include a dollar sign in the ticks
              callback: function (value, index, ticks) {
                return value + 's';
              }
            }
          }
        }
      }
    }
  );
})();
