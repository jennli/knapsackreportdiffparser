# KnapsackReportDiffParser

This project allows team members to compare test duration differences between two Knapsack reports in a local Stacked Bar chart.


## Getting started

1. clone this project to your local

2. Download the Knapsack reports including report-master.json and at least one specific job report json to the `json` folder. For example, in https://gitlab.com/gitlab-org/gitlab/-/jobs/4829436240/artifacts/browse/knapsack/, the two files you may want to download are `report-master.json` and `rspec-ee_unit_pg14_es8_9_18_report.json`.

For demo purpose, the project includes 2 default reports in the json folder already.

3. in the project root dir, run the following command to prepare data from the demo jsons:

```ruby
ruby knapsack_json_diff_parser.rb
``` 
or the following command to prepare data from `my-job-report.json` and `my-master-report.json` (the artifact json downloaded from step 2).

```ruby
ruby knapsack_json_diff_parser.rb my-job-report.json my-master-report.json
``` 

4. in the project root dir, run:

```bash
yarn build
yarn dev
``` 

then you can see the generated bar chart via http://localhost:1234/.
