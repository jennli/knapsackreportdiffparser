require 'json'

class KnapsackReportDiffParser
  attr_reader :master_report_json, :actual_job_report_json

  def initialize(master_report, actual_job_report)
    @master_report_json = read_json(master_report)
    @actual_job_report_json = read_json(actual_job_report)
  end

  def parse_input_array
    expected = JSON.parse(master_report_json)
    actual = JSON.parse(actual_job_report_json)

    result = { 'data' => [] }

    actual.each do |key, value|
      input_data = {
        'test_file': key,
        'expected': expected[key],
        'actual': actual[key]
      }

      result['data'].push(input_data)
    end

    result
  end

  def read_json(*paths)
    File.read(File.join("#{__dir__}/json", *paths))
  end
end

job_report = ARGV[0] || 'rspec-ee_unit_pg14_es8_9_18_report-example.json'
master_report = ARGV[1] || 'report-master-example.json'

parser = KnapsackReportDiffParser.new(master_report, job_report)
output = parser.parse_input_array

File.open('chart_data/report-diff.json', 'w') do |f|
  f.write(output.to_json)
end
